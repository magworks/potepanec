module ProductsHelper
  def active_title(color: nil)
    if color.nil?
      "Products"
    else
      "Products of #{color}"
    end
  end
end
