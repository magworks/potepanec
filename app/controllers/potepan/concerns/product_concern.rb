module Potepan::Concerns::ProductConcern
  extend ActiveSupport::Concern

  def hash_classification_by_filter(filter_option)
    {
      labels: Spree::OptionType.names_classification_by_filter(filter_option),
      counts: Spree::Variant.counts_classification_by_filter(filter_option),
    }
  end
end
