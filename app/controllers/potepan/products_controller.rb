class Potepan::ProductsController < ApplicationController
  include Potepan::Concerns::ProductConcern

  def index
    @taxonomies = Spree::Taxonomy.includes_taxons
    @color = params[:color]
    if @color.nil?
      @variants = Spree::Variant.includes_option_values_price_images.page(params[:page])
    else
      @variants = Spree::Variant.variants_classification_by_filter('Color', @color).
        page(params[:page])
    end
    @hash_classification_by_filter = hash_classification_by_filter('Color')
  end

  def show
    @product = Spree::Product.
      includes(:variants_including_master, { option_types: :option_values }, :properties).
      find(params[:id])
    @related_products = Spree::Product.
      includes_master_price_images.with_related_taxons(@product).excluding(@product)
  end
end
