class Potepan::StaticPagesController < ApplicationController
  def index
    @featured_products = Spree::Product.available_order.limit_products
  end
end
