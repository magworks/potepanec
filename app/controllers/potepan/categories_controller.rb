class Potepan::CategoriesController < ApplicationController
  include Potepan::Concerns::ProductConcern

  def show
    @taxon = Spree::Taxon.find(params[:id])
    @taxonomies = Spree::Taxonomy.includes_taxons
    @products_of_taxon = Spree::Product.joins(:taxons).
      includes_master_price_images.where(spree_taxons: { id: @taxon.id }).
      references(:master, :default_price, :images).page(params[:page])

    # サイドバーの分類
    @hash_classification_by_filter = hash_classification_by_filter('Color')
  end
end
