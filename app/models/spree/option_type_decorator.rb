module Spree
  OptionType.class_eval do
    def self.names_classification_by_filter(filter_name)
      joins(:option_values).
        where(presentation: filter_name).
        select('spree_option_values.name, spree_option_values.id').
        order('name DESC')
    end
  end
end
