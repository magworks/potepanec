module Spree
  Taxonomy.class_eval do
    scope :includes_taxons, -> { includes(taxons: :children) }
  end
end
