module Spree
  Variant.class_eval do
    scope :includes_option_values_price_images, -> {
      includes({ option_values: :option_type }, :default_price, :images)
    }
    scope :price_order, -> (order_option: :desc) {
      order("spree_prices.amount #{order_option}")
    }
    scope :available_order, -> (order_option: :desc) {
      order("spree_products.available_on #{order_option}")
    }
    scope :not_master, -> { where(is_master: false) }
    scope :includes_images, -> { includes(:images) }

    def self.counts_classification_by_filter(filter_name)
      # filter_nameによる分類の商品バリエーション各数量
      includes({ option_values: :option_type }, :default_price, :images).
        where(spree_option_types: { presentation: filter_name }).
        references(:option_values, :option_type, :default_price, :images).
        group('spree_option_values.name').count
    end

    def self.variants_classification_by_filter(filter_name, params_keyword)
      # filter_nameによる分類で、選択したparamsの商品バリエーション
      joins(option_values: :option_type).includes(:default_price, :images).
        where(spree_option_types: { presentation: filter_name }).
        where(spree_option_values: { name: params_keyword }).
        references(:default_price, :images)
    end
  end
end
