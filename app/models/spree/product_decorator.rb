module Spree
  Product.class_eval do
    scope :includes_master_price_images, -> {
      includes({ master: [:default_price, :images] })
    }
    scope :with_related_taxons, -> (product) {
      joins(:taxons).
        where(spree_taxons: { id: product.taxon_ids }).
        limit(Constants::RELATED_PRODUCTS_LIMIT)
    }
    scope :excluding, -> (product) { where.not(id: product.id).distinct }
    scope :limit_products, -> { limit(Constants::FEATURED_PRODUCTS_LIMIT) }
    scope :available_order, -> (order_option: :desc) {
      order(available_on: order_option)
    }
  end
end
