require 'rails_helper'
RSpec.describe Spree::Variant, type: :model do
  include_context '関連付けられているproduct,variant,option_type,option_valueが存在するとして'

  describe '.counts_classification_by_filter(filter_name)' do
    context "'Color'タイプであるとき" do
      let(:option_type) { Spree::OptionType.first }

      it_behaves_like "OptionTypeのpresentation属性は'Color'が存在する" # presentationカラムを、whereで絞り込むことをテスト
      it "その分類に続する商品バリエーションがある（数が存在する）" do # 関連先のoption_valueのname属性をグループ化しカウントすることをテスト
        expect(
          Spree::Variant.counts_classification_by_filter('Color').values
        ).to contain_exactly(3, 3, 4)
      end
    end

    context "'Color'タイプでないとき" do
      let(:option_values_color_counts) { Spree::Variant.counts_classification_by_filter('Color') }

      it "その分類および分類数はColorタイプものでない" do
        expect(
          Spree::Variant.counts_classification_by_filter('Size')
        ).not_to eq option_values_color_counts
      end
    end
  end

  describe '.variants_classification_by_filter(filter_name, params_keyword)' do
    context "'Color'タイプであるとき" do
      before do
        option_type = Spree::OptionType.find_by!(presentation: 'Color')
        @option_value_first = option_type.option_values[0]
        @got_variants = Spree::Variant.find_by(id: @option_value_first.variants[0].id)
      end
      it_behaves_like "OptionTypeのpresentation属性は'Color'が存在する" # presentationカラムを、whereで絞り込むことをテスト
      it "その分類に続する商品バリエーションがある（variantが存在する）" do # 関連先のoption_valueのname属性を使ったwhereの絞り込みをテスト
        expect(
          Spree::Variant.variants_classification_by_filter(
            'Color',
            @option_value_first.name
          ).first
        ).to eq @got_variants
      end
    end

    context "'Color'タイプでないとき" do
      before do
        option_type = Spree::OptionType.find_by!(presentation: 'Size')
        @option_value_first = option_type.option_values[0]
        @got_variants = Spree::Variant.find_by(id: @option_value_first.variants[0].id)
      end
      it "'Color'タイプでない分類の商品バリエーションが表示される" do
        expect(
          Spree::Variant.variants_classification_by_filter(
            'Size',
            @option_value_first.name
          ).first
        ).to eq @got_variants
      end
    end
  end
end
