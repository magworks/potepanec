require 'rails_helper'
RSpec.describe Spree::Product, type: :model do
  describe '関連付けられているproduct,taxon,taxonomyが存在するとして' do
    before { @spree_product = create(:spree_product_with_taxon_and_taxonomy) }

    describe '.with_related_taxons' do
      context '商品ページで類似カテゴリー商品があるとき' do
        context '類似カテゴリー商品数が１つのとき' do
          before { create(:spree_product_with_taxon_and_taxonomy) }
          it "類似カテゴリー商品は有効である" do
            expect(Spree::Product.with_related_taxons(@spree_product).first).to be_valid
          end
          it "類似カテゴリー商品数は既定値(RELATED_PRODUCTS_LIMIT)以下である" do
            expect(Spree::Product.with_related_taxons(@spree_product).size).to be <=
              Constants::RELATED_PRODUCTS_LIMIT
          end
        end

        context '類似カテゴリー商品数が規定値以上のとき' do
          before do
            create_list(
              :spree_product_with_taxon_and_taxonomy,
              Constants::RELATED_PRODUCTS_LIMIT + 1
            )
          end
          it "類似カテゴリー商品数は既定値(RELATED_PRODUCTS_LIMIT)である" do
            expect(
              Spree::Product.with_related_taxons(@spree_product).size
            ).to eq Constants::RELATED_PRODUCTS_LIMIT
          end
        end
      end

      context '商品ページで類似カテゴリー商品がないとき' do
        before { @other_products = create_list(:spree_product_with_taxon_and_taxonomy, 3) }
        it "類似カテゴリー商品は含まれない" do
          expect(Spree::Product.with_related_taxons(@spree_product).
            pluck(:id)).not_to contain_exactly(@other_products.pluck(:id))
        end
      end
    end

    describe '.excluding' do
      context "自身の商品を除外したとき" do
        it "自身は含まない" do
          expect(Spree::Product.excluding(@spree_product)).not_to include(@spree_product)
        end
      end
    end

    describe '.limit_products' do
      context '新着商品数が既定値(FEATURED_PRODUCTS_LIMIT)以下であるとき' do
        before { create_list(:spree_product, 5) }
        it "商品表示数は既定値以下である" do
          expect(Spree::Product.limit_products.size).to be <= Constants::FEATURED_PRODUCTS_LIMIT
        end
      end

      context '新着商品数が既定値(FEATURED_PRODUCTS_LIMIT)以上であるとき' do
        before { create_list(:spree_product, 10) }
        it "商品表示数は既定値である" do
          expect(Spree::Product.limit_products.size).to match Constants::FEATURED_PRODUCTS_LIMIT
        end
      end
    end

    describe '.available_order' do
      before { create_list(:spree_product, 5) }
      context '登録日（available_on）によるASC(昇順)であるとき' do
        before { @products = Spree::Product.available_order(order_option: :asc) }
        it "商品のトップは古着である" do
          expect(@products.first.available_on).to be < @products.last.available_on
        end
      end

      context '登録日（available_on）によるDESC(降順)であるとき' do
        before { @products = Spree::Product.available_order }
        it "商品のトップは新着である" do
          expect(@products.first.available_on).to be > @products.last.available_on
        end
      end
    end
  end
end
