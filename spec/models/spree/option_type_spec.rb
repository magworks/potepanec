require 'rails_helper'
RSpec.describe Spree::OptionType, type: :model do
  include_context '関連付けられているproduct,variant,option_type,option_valueが存在するとして'

  describe '.option_type_filter' do
    context "'Color'タイプであるとき" do
      let(:option_type_color) { Spree::OptionType.names_classification_by_filter('Color') }

      before do
        @option_values_name =
          Spree::OptionType.find_by(presentation: 'Color').
            option_values.pluck('spree_option_values.name')
      end
      it "その分類は降順（DESC)である" do # 自身(option_type)のnameを降順(DESC)でならべかえることをテスト
        expect(option_type_color.first.name).to be > option_type_color.last.name
      end
      it_behaves_like "OptionTypeのpresentation属性は'Color'が存在する" # presentationカラムを、whereで絞り込むことをテスト
      it "その分類名は存在する" do # 関連先のoption_valueの名前とidをセレクトしていることをテスト
        expect(Spree::OptionType.names_classification_by_filter('Color').
          pluck('spree_option_values.name')).to match_array @option_values_name
      end
    end

    context "'Color'タイプでないとき" do
      before do
        @option_values_name =
          Spree::OptionType.find_by(presentation: 'Size').
            option_values.pluck('spree_option_values.name')
      end
      it "その分類名はColorタイプの分類名ではない" do
        expect(Spree::OptionType.names_classification_by_filter('Color').
          pluck('spree_option_values.name')).not_to match_array @option_values_name
      end
    end
  end
end
