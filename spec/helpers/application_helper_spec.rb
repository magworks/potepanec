require 'rails_helper'
RSpec.describe ApplicationHelper, type: :helper do
  describe '#full_title' do
    let(:base_title) { "PotepanEC" }
    let(:full_title) { helper.full_title(page_title: page_title) }

    context "ページタイトルが空である場合" do
      let(:page_title) { '' }

      it "ページタイトルは表示されない" do
        expect(full_title).to eq base_title
      end
    end

    context "ページタイトルが空である場合" do
      let(:page_title) { '' }

      it "ベースタイトルは表示される" do
        expect(full_title).to eq base_title
      end
    end

    context "ページタイトルが空でない場合" do
      let(:page_title) { "PageTitle" }

      it "ページタイトルは表示される" do
        expect(full_title).to include page_title
      end
    end

    context "ページタイトルが空でない場合" do
      let(:page_title) { "PageTitle" }

      it "ベースタイトルは表示される" do
        expect(full_title).to include base_title
      end
    end
  end
end
