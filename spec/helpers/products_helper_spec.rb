require 'rails_helper'
RSpec.describe ProductsHelper, type: :helper do
  describe '#active_title' do
    context "colorが'red'のとき" do
      let(:color) { 'red' }

      it "ページタイトルは'Products of red'となる" do
        expect(helper.active_title(color: 'red')).to eq 'Products of red'
      end
    end

    context "colorがnilのとき" do
      let(:color) { '' }

      it "ページタイトルは'Products'となる" do
        expect(helper.active_title(color: nil)).to eq 'Products'
      end
    end
  end
end
