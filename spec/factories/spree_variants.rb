FactoryGirl.define do
  factory :spree_variant, class: "Spree::Variant" do
    price 19.99
    sku ""
    is_master 0
    track_inventory true
    sequence(:created_at) { |n| "#{n.days.ago.to_datetime}" }
    # association :product, factory: :spree_product
    # before(:create) { create(:stock_location) if Spree::StockLocation.count == 0 }

    trait :with_option_value do
      after(:create) do |spree_variant|
        create(
          :spree_option_values_variants,
          variant: spree_variant,
          option_value: create(:spree_option_value)
        )
      end
    end

    factory :spree_variant_with_option_value, traits: [:with_option_value]
  end
end
