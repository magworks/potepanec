FactoryGirl.define do
  factory :spree_option_value, class: "Spree::OptionValue" do
    sequence(:name, 1) { |n| "value#{n}" }
    sequence(:presentation, 1) { |n| "Value#{n}" }
    association :option_type, factory: :spree_option_type
  end
end
