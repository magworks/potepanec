FactoryGirl.define do
  factory :spree_product, class: "Spree::Product" do
    sequence(:name) { |n| "Product ##{n}" }
    description "As seen on TV!"
    price 19.99
    sequence(:available_on) { |n| "#{n.days.ago.to_datetime}" }
    deleted_at nil
    shipping_category { |r| r.association(:spree_shipping_category) }
    sequence(:created_at) { |n| "#{n.days.ago.to_datetime}" }

    trait :with_option_type do
      after(:create) do |spree_product|
        create(
          :spree_product_option_type,
          product: spree_product,
          option_type: create(:spree_option_type_with_option_value, presentation: 'Color')
        )
      end
    end

    trait :with_taxon_and_taxonomy do
      after(:create) do |spree_product|
        taxonomy = Spree::Taxon.find_by(name: 'Category')
        if taxonomy.present?
          parent_taxon = taxonomy.root
          child_taxon1 = Spree::Taxon.find_by(name: 'Child 1')
          spree_product.taxons << parent_taxon
          spree_product.taxons << child_taxon1
        else
          taxonomy = create(:spree_taxonomy, name: 'Category')
          parent_taxon = taxonomy.root
          child_taxon1 =
            create(
              :spree_taxon,
              name: 'Child 1',
              taxonomy_id: taxonomy.id,
              parent: parent_taxon
            )
          spree_product.taxons << parent_taxon
          spree_product.taxons << child_taxon1
        end
      end
    end

    factory :spree_product_with_option_type, traits: [:with_option_type]
    factory :spree_product_with_taxon_and_taxonomy, traits: [:with_taxon_and_taxonomy]
  end
end
