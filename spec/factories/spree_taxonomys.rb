FactoryGirl.define do
  factory :spree_taxonomy, class: "Spree::Taxonomy" do
    name 'Brand'
  end
end
