FactoryGirl.define do
  factory :spree_option_type, class: "Spree::OptionType" do
    sequence(:name, 1) { |n| "type#{n}" }
    presentation 'Color-Filter'

    trait :with_option_value do
      after(:create) do |spree_option_type|
        create_list(:spree_option_value, 4, option_type: spree_option_type)
      end
    end

    factory :spree_option_type_with_option_value, traits: [:with_option_value]
  end
end
