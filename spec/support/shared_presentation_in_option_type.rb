RSpec.shared_examples_for "OptionTypeのpresentation属性は'Color'が存在する" do
  it { expect(Spree::OptionType.first).to have_attributes(presentation: 'Color') }
end
