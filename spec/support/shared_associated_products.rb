RSpec.shared_context '関連付けられているproduct,variant,option_type,option_valueが存在するとして' do
  before do
    # productが2,関連付いたoption_typeが2,option_valueが8,variantが10を生成
    product = create(:spree_product_with_option_type) # tshirt
    product2 = create(:spree_product) # bag
    product.variants << Spree::Variant.first
    product2.variants << Spree::Variant.first
    product.variants << create_list(:spree_variant, 9, product: product)

    option_type_color = Spree::OptionType.find_by!(presentation: 'Color')
    option_type_size = create(:spree_option_type_with_option_value, presentation: 'Size')
    product.option_types << option_type_size

    product.variants[0].option_values <<
      [
        option_type_color.option_values[0],
        option_type_size.option_values[0],
      ] # tシャツ赤サイズS
    product.variants[1].option_values <<
      [
        option_type_color.option_values[0],
        option_type_size.option_values[1],
      ] # tシャツ赤サイズM
    product.variants[2].option_values <<
      [
        option_type_color.option_values[0],
        option_type_size.option_values[2],
      ] # tシャツ赤サイズL
    product.variants[3].option_values <<
      [
        option_type_color.option_values[1],
        option_type_size.option_values[0],
      ] # tシャツ青サイズS
    product.variants[4].option_values <<
      [
        option_type_color.option_values[1],
        option_type_size.option_values[1],
      ] # tシャツ青サイズM
    product.variants[5].option_values <<
      [
        option_type_color.option_values[1],
        option_type_size.option_values[2],
      ] # tシャツ青サイズL
    product.variants[6].option_values <<
      [
        option_type_color.option_values[2],
        option_type_size.option_values[0],
      ] # tシャツ緑サイズS
    product.variants[7].option_values <<
      [
        option_type_color.option_values[2],
        option_type_size.option_values[1],
      ] # tシャツ緑サイズM
    product.variants[8].option_values <<
      [
        option_type_color.option_values[2],
        option_type_size.option_values[2],
      ] # tシャツ緑サイズL
    product.variants[9].option_values <<
      [
        option_type_color.option_values[2],
        option_type_size.option_values[3],
      ] # tシャツ緑サイズXL
  end
end
