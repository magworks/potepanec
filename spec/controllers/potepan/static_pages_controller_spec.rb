require 'rails_helper'
RSpec.describe Potepan::StaticPagesController, type: :controller do
  describe 'GET #index' do
    render_views
    before { get :index }

    it 'HTTPレスポンスがステータスコード200(:success)を返すこと' do
      expect(response).to have_http_status(:success)
    end
    it 'indexテンプレートを表示すること' do
      expect(response).to render_template(:index)
    end
    it 'HTTPレスポンスのbodyに"新着商品"が含まれること' do
      expect(response.body).to include('新着商品')
    end
  end
end
