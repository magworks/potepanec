require 'rails_helper'
RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'GET #index' do
    render_views
    before { get :index }

    it 'HTTPレスポンスがステータスコード200(:success)を返すこと' do
      expect(response).to have_http_status(:success)
    end
    it 'indexテンプレートを表示すること' do
      expect(response).to render_template(:index)
    end
  end

  describe 'GET #show' do
    render_views
    let(:spree_product) { create(:spree_product) }

    before { get :show, { :params => { :id => spree_product.id } } }

    it 'HTTPレスポンスがステータスコード200(:success)を返すこと' do
      expect(response).to have_http_status(:success)
    end
    it 'showテンプレートを表示すること' do
      expect(response).to render_template(:show)
    end
    it 'HTTPレスポンスのbodyに"関連商品"が含まれること' do
      expect(response.body).to include('関連商品')
    end

    it "要求された商品が@productに割り当られていること" do
      expect(assigns(:product)).to eq spree_product
    end
  end
end
