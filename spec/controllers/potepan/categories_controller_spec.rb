require 'rails_helper'
RSpec.describe Potepan::CategoriesController, type: :controller do
  describe 'GET #show' do
    render_views
    let(:spree_taxon) { create(:spree_taxon) }

    before { get :show, { :params => { :id => spree_taxon.id } } }

    it 'HTTPレスポンスがステータスコード200(:success)を返すこと' do
      expect(response).to have_http_status(:success)
    end
    it 'showテンプレートを表示すること' do
      expect(response).to render_template(:show)
    end
    it 'HTTPレスポンスのbodyに"#{spree_taxon.name}"が含まれること' do
      expect(response.body).to include("#{spree_taxon.name}")
    end
  end
end
