require 'spec_helper'
RSpec.describe Potepan::Concerns::ProductConcern, type: :controller do
  include_context '関連付けられているproduct,variant,option_type,option_valueが存在するとして'
  let(:test_class) { Struct.new(:test_instance) { include Potepan::Concerns::ProductConcern } }
  let(:test_instance) { test_class.new }

  describe '.hash_classification_by_filter(filter_option)' do
    before do
      @value_be_size = test_instance.hash_classification_by_filter('Size').values
    end

    context 'filter_optionがColorタイプであるとき' do
      it '戻り値はハッシュとして存在する' do
        expect(test_instance.hash_classification_by_filter('Color').keys).to eq [:labels, :counts]
        expect(test_instance.hash_classification_by_filter('Color').values).not_to be_nil
      end
    end

    context 'filter_optionがColorタイプでないとき' do
      it '戻り値はColorタイプの値ではない' do
        expect(test_instance.hash_classification_by_filter('Color').values).not_to eq @value_be_size
      end
    end
  end
end
