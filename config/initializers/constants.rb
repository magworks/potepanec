module Constants
  # 関連商品数
  RELATED_PRODUCTS_LIMIT = 4
  # 新着商品数
  FEATURED_PRODUCTS_LIMIT = 8
end
